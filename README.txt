
user: admin
pass: usuario123456

user: yomogan
pass: 123

--------------------------------------------

Hola me llamo cesar y este es mi tercer proyecto, esta basado en un framework de php orientado a objetos con Angular
(este proyecto es una migracion del proyecto en JQuery)

Esta es una pagina de criptomonedas.

---------------------------------------------

HOME

La pagina HOME tiene un carrusel y debajo un buscador que puedes buscar cualquier criptomoneda
que te llevara a unos detalles sobre la misma recogidos mediante una api, si la criptomoneda no esta registrada en
BD se informara que no existe esta criptomoneda.
Debajo, encontramos categorias de criptomonedas ordenadas por las mas visitadas, con un boton debajo
que nos permite visualizar mas categorias, llegado al maximo de categorias un toastr nos informa que no quedan mas.

----------------------------------------------

LIST

Cuando pulsamos una categoria en el HOME, recogemos de BD las monedas guardadas y las pintamos por mas visitadas, debajo de estas criptomonedas tenemos un pager que nos permite visualizar mas criptomonedas ocultas.Tambien se guarda en BD que ha sido visitada dicha categoria.
Debajo de todas estas criptomonedas tenemos un mapa el cual pinta una ubicacion de cada criptomoneda de dicha generacion, generada automaticamente y que esta guardada en BD.

----------------------------------------------

DETAILS

Una vez pulsamos una criptomoneda del list anterior, podemos encontrar los detalles sobre dicha criptomoneda. Tambien se guarda en BD que ha sido visitada dicha criptomoneda y dicha categoria (actualmente hay un bug y de normal no se podra visualizar, pulsando en el buscador sin inertar ninguna criptomoneda se notificara que dicha criptomoneda (porque no hemos escrito ninguna) pero debajo se visualizaràn los detalles).

---------------------------------------------

SHOP

Shop seria basicamente lo mismo que el list pero con todas las criptomonedas, aqui no hay mapa ya que al haber 60 criptomonedas no queria saturar el mapa. Una vez pulsamos una criptomoneda seria igual que el details anterior.Tambien se guarda en BD que ha sido visitada dicha criptomoneda y dicha categoria.


--------------------------------------------

Create user

Actualmente esta desabilitado por temas de tiempo

--------------------------------------------

CONTACT

Tenemos para seleccionar el motivo de la consulta y luego escribimos la consulta, una vez lo enviamos si todo ha ido bien un toastr nos notificara que todo ha ido bien, y recibiremos al correo especificado una copia de la consulta.

--------------------------------------------

PROFILE

Este modulo solo esta disponible para usuarios registrados, nos muestra un poco de informacion sobre el usuario y esta la podemos cambiar, una vez guardado seremos redireccionados al home con un toastr indicandonos que todo fue correcto.

--------------------------------------------

LOGIN

Las contraseñas del login se cifran con SHA256 para ser comparadas con BD, el register guarda la contraseña en SHA256.
En este modulo, podemos visualizar un apartado de login, otro de register y otro de singin con social media. Si nos registramos recibiremos un correo para activar nuestra cuenta con un token y con esto sabemos que usuario es, una vez verificado desde el email seremos redireccionados al home con la cuenta activada y logeados, si nos logeamos, y todo ha ido bien nos notificara con un toastr y seremos redireccionados al home, tambien se generara un token y serà actualizado en BD.
Con el singin social si cuando nos logeamos frente a google o twiter, si el usuario no se encuentra en BD serà registrado, si no, serà logeado, actualizando un token generado aleatoriamente
Tambien podemos hacer un recover password, pulsamos en el "he olvidado mi contraseña" y seremos redireccionados a un formulario de correo y contraseña, ponemos nuestro correo, nuestra contraseña nueva y recibiremos un correo con un link con un token para validar el cambio de contraseña, asi mismo seremos redireccionados al home con la sesion iniciada.
Tambien, cuando nos logeamos aparece en el menu, aparte de PROFILE, aparece LogOut y login desaparece, ya que si el usuario ya esta logeado no tiene necesidad de ir al login, cuando le damos al LOGOUT, nos redirecciona al login, y quita del menu PROFILE Y LOGOUT y pone el login, tambien elimina del localstorage el token que se habia guardado cuando nos hemos logeado (este token cuando nos logeamos se guarda ya sea por singin social o login normal (este token tambien esta en BD para ser comparado en cualquier momento asi sabemos que usuario esta logeado))
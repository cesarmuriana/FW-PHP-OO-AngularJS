<?php

//session_start();

    /*if (!isset($_POST['op']))
        $_POST['op']='index';*/

class controller_home{
        function __construct() {
            $_SESSION['module'] = "home";
        }
        
    	function get_categories(){
    			$categories = loadModel(MODEL_HOME, "home_model", "get_categories");
    			echo json_encode($categories);
    			die();
    		}
    	function save_class(){
    			$_SESSION['clase']=$_GET['param'];
    			die();
    		}
    	function get_class(){
    			echo json_encode($_SESSION['clase']);
    			die();
    		}
    	function get_monedas(){
    			$monedas = loadModel(MODEL_HOME, "home_model", "get_monedas", $_POST);
    			echo json_encode($monedas);
    			die();
    		}
        function get_all_monedas(){
                $monedas = loadModel(MODEL_HOME, "home_model", "get_all_monedas");
                echo json_encode($monedas);
                die();
            }
    	function save_moneda(){
    			$_SESSION['moneda']=$_GET['param'];
                echo json_encode($_SESSION['moneda']);
    			die();
    		}
    	function get_moneda(){
    			echo json_encode($_SESSION['moneda']);
    			die();
    		}
        function get_monedas_by_cat(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "get_monedas_by_cat", $_GET['param']);
                echo json_encode($busqueda);
                die();
            }
            
        function get_busqueda(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "get_busqueda", $_POST['busqueda']);
                echo json_encode($busqueda);
                die();
            }
        function get_moneda_by_name(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "get_moneda", $_GET['param']);
                echo json_encode($busqueda);
                die();
            }
        function popularity_categories(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "popularity_cat", $_GET['param']);
                print_r($busqueda);
                die();
            }
        function popularity_crypto(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "popularity_mon", $_GET['param']);
                print_r($busqueda);
                die();
            }
        function num_pages(){
                $item_per_page = 4;
                try {
                    //throw new Exception();
                    $total_monedas = loadModel(MODEL_HOME, "home_model", "count_total_mon_by_cat", $_POST['clase']);
                    $get_total_rows = $total_monedas[0]["total"]; //total records
                    $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
                } catch (Exception $e) {
                    showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                }

                if ($get_total_rows) { 
                    $jsondata["pages"] = $pages;
                    echo json_encode($jsondata);
                    exit;
                }
            }
        function num_pages_all(){
                $item_per_page = 4;
                try {
                    //throw new Exception();
                    $total_monedas = loadModel(MODEL_HOME, "home_model", "count_total_mon");
                    $get_total_rows = $total_monedas[0]["total"]; //total records
                    $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
                } catch (Exception $e) {
                    showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                }

                if ($get_total_rows) { 
                    $jsondata["pages"] = $pages;
                    echo json_encode($jsondata);
                    exit;
                }
            }

        function set_comentarios(){
            print_r($_GET);
            die();
        }
        function get_comentarios(){
            print_r($_GET);
            die();
        }
        function set_likes(){
            $retorno = [];

            $usuario = json_decode($_GET['param']);
            $usuario_completo = json_decode(json_encode($usuario), true);

            $usuario = $usuario_completo[1]["idusuario"];
            $moneda = $usuario_completo[0]["criptomoneda"];
            $sentencia = "select * from likes where moneda='$moneda' and idusuario='$usuario'";

            $busqueda = loadModel(MODEL_HOME, "home_model", "buscar", $sentencia );
            if ($busqueda){
                $mensaje = "Esta cripto ya esta en su lista de likes";
                $estado = false;
                array_push($retorno, $mensaje);
                array_push($retorno, $estado);
            }else{
                $sentencia = "INSERT INTO `FW_OO_PHP`.`likes` (`moneda`,`idusuario`) VALUES ('$moneda','$usuario')";
                $insertar = loadModel(MODEL_HOME, "home_model", "insertar", $sentencia );
                if ($insertar){
                    $mensaje = "Like agregado correctamente";
                    $estado = true;
                    array_push($retorno, $mensaje);
                    array_push($retorno, $estado);
                }else{
                    $mensaje = "Ha habido un error";
                    $estado = false;
                    array_push($retorno, $mensaje);
                    array_push($retorno, $estado);
                }
                
            }

            echo json_encode($retorno);
            die();
        }
        function get_likes(){
            $retorno = [];

            $usuario = json_decode($_GET['param']);
            $usuario_completo = json_decode(json_encode($usuario), true);

            $usuario = $usuario_completo[0]["idusuario"];

            $sentencia = "select * from likes where idusuario='$usuario'";
            $busqueda = loadModel(MODEL_HOME, "home_model", "buscar", $sentencia);
            if ($busqueda){
                foreach ($busqueda as $moneda){
                    array_push($retorno, $moneda['moneda']);
                }
                $estado = true;
                array_push($retorno, $estado);
            }else{
                $mensaje = "No tiene ninguna moneda preferida";
                $estado = false;
                array_push($retorno, $mensaje);
                array_push($retorno, $estado);
                }

            echo json_encode($retorno);
            die();
        }

        function set_rate(){
            $retorno = [];

            $usuario = json_decode($_GET['param']);
            $usuario_completo = json_decode(json_encode($usuario), true);


            $usuario = $usuario_completo[1]["idusuario"];
            $moneda = $usuario_completo[0]["criptomoneda"];
            $rate = $usuario_completo[2];

            $sentencia = "select * from rate where moneda='$moneda' and idusuario='$usuario'";

            $busqueda = loadModel(MODEL_HOME, "home_model", "buscar", $sentencia );
            if ($busqueda){
                $sentencia = "UPDATE `FW_OO_PHP`.`rate` set puntuacion='$rate' where idusuario='$usuario'";
                $insertar = loadModel(MODEL_HOME, "home_model", "insertar", $sentencia );
                if ($insertar){
                    $mensaje = "Se ha actuaizado tu calificacion";
                    $estado = true;
                    array_push($retorno, $mensaje);
                    array_push($retorno, $estado);
                }else{
                    $mensaje = "Ha habido un error al actualizar";
                    $estado = false;
                    array_push($retorno, $mensaje);
                    array_push($retorno, $estado);
                }
                
            }else{
                $sentencia = "INSERT INTO `FW_OO_PHP`.`rate` (`moneda`,`idusuario`,`puntuacion`) VALUES ('$moneda','$usuario', '$rate')";
                $insertar = loadModel(MODEL_HOME, "home_model", "insertar", $sentencia );
                if ($insertar){
                    $mensaje = "Se ha calificado correctamente";
                    $estado = true;
                    array_push($retorno, $mensaje);
                    array_push($retorno, $estado);
                }else{
                    $mensaje = "Ha habido un error al guardar tu calificacion";
                    $estado = false;
                    array_push($retorno, $mensaje);
                    array_push($retorno, $estado);
                }
                
            }

            echo json_encode($retorno);
            die();
        }

        function get_rate(){
            $retorno = [];

            $usuario = json_decode($_GET['param']);
            $usuario_completo = json_decode(json_encode($usuario), true);

            $usuario = $usuario_completo[0]["idusuario"];

            $sentencia = "select * from rate where idusuario='$usuario'";
            $busqueda = loadModel(MODEL_HOME, "home_model", "buscar", $sentencia);
            if ($busqueda){
                foreach ($busqueda as $moneda){
                    array_push($retorno, $moneda['moneda']);
                    array_push($retorno, $moneda['puntuacion']);
                }
                $estado = true;
                array_push($retorno, $estado);
            }else{
                $mensaje = "No tiene ninguna moneda preferida";
                $estado = false;
                array_push($retorno, $mensaje);
                array_push($retorno, $estado);
                }

            echo json_encode($retorno);
            die();
        }

        function count_rates(){
            $retorno = [];

            $moneda = ($_GET['param']);

            $sentencia = "select count(*) as cantidad from rate where moneda='$moneda'";
            $media = "select avg(puntuacion) as media from rate where moneda='$moneda'";

            $cantidad = loadModel(MODEL_HOME, "home_model", "buscar", $sentencia );
            $media = loadModel(MODEL_HOME, "home_model", "buscar", $media );
            if ($cantidad){
                array_push($retorno, $cantidad[0]['cantidad']);
                array_push($retorno, $media[0]['media']);
            }else{
                $mensaje = "Ha habido un error recoger el total";
                $estado = false;
                array_push($retorno, $mensaje);
                array_push($retorno, $estado);
                
            }

            echo json_encode($retorno);
            die();
        }
    }
<?php
class controller_login{
        function __construct() {
            //include(UTILS . "common.inc.php");
        }
        function getuserbytoken(){
            $user = json_decode($_GET['param']);
            $busqueda=[];
            array_push($busqueda, "token");
            array_push($busqueda, $user);
            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            echo json_encode($finduser);
        }

        function login_google(){
            $usuario = json_decode($_GET['param']);

            $retorno=[];
            $busqueda=[];

            $uid=$usuario[4];
            $user=$usuario[1];
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($retorno,$token);
            array_push($usuario, $token);
            $exist_user=$usuario[4];
            array_push($busqueda, "idusuario");
            array_push($busqueda, $exist_user);
            $usuario= [];
            array_push($usuario, $uid);
            array_push($usuario, $token);

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            if ($finduser){
                $login = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($login){
                    array_push($retorno,"Login succefull");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Error on login");
                }
            }else{
                $usuario = json_decode($_GET['param']);
                $uid=$usuario[4];
                $user=$usuario[1];
                array_push($usuario, $token);
                array_push($usuario, "");
                array_push($usuario, "1");
                $save = loadModel(MODEL_LOGIN, "login_model", "save_login", $usuario);
                if ($save){
                    array_push($retorno,"Registro correcto");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Ha habido un error en el registro del usuario");
                }
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }           
        function login_twiter(){
            $usuario = json_decode($_GET['param']);
            $userfind = [];
            $retorno=[];
            $busqueda=[];

            $user=$usuario[1];
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($retorno,$token);
            array_push($userfind, $usuario[4]);
            array_push($userfind, $token);
            
            $exist_user=$usuario[4];
            array_push($usuario, $token);
            array_push($busqueda, "idusuario");
            array_push($busqueda, $exist_user);

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            if ($finduser){
                $login = loadModel(MODEL_LOGIN, "login_model", "login_user", $userfind);
                if ($login){
                    array_push($retorno,"Login succefull");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Error on login");
                }
            }else{
                $save = loadModel(MODEL_LOGIN, "login_model", "save_login", $usuario);
                if ($save){
                    array_push($retorno,"Registro correcto");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Ha habido un error en el registro del usuario");
                }
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }

        function login_own(){
            $usuario = json_decode($_GET['param']);
            $retorno=[];
            $busqueda=[];

            $user=$usuario[0];
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($retorno,$token);
            array_push($usuario, $token);
            $exist_user=$usuario[0];
            $password=$usuario[1];
            $passwrd = hash("sha256" , $password);
            
            array_push($busqueda, "pass='$passwrd' and activate=1 and username");
            array_push($busqueda, $exist_user);
            $user = $usuario[0];

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            if ($finduser){
                $usuario = [];
                array_push($usuario,$user);
                array_push($usuario,$token);
                $login = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($login){
                    array_push($retorno,"Login succefull");
                    $_SESSION['usuario']=$usuario[0];
                    if ($finduser[0]['img']!='')
                        $_SESSION['imagen']=$finduser[0]['img'];
                        $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Error on login");
                }
            }else{
                array_push($retorno,"La contraseña o el usuario es incorrecto, puede que tampoco haya validado su email");
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }
        
        function register_own(){
            $usuario = json_decode($_GET['param']);
            $retorno=[];
            $busqueda=[];

            $user=$usuario[0];
            $email=$usuario[1];
            $passwrd=$usuario[2];
            $passwrd = hash("sha256" , $passwrd);
            array_push($usuario,$passwrd);
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($busqueda, "nameuser");
            array_push($busqueda, $user);

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            
            if (!$finduser){
                $busqueda=[];
                array_push($busqueda, "email");
                array_push($busqueda, $email);
                $findemail = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
                if ($findemail){
                    array_push($retorno, "El e-mail ya existe");
                }else{
                    $usuario = [];
                    array_push($usuario,$user);
                    array_push($usuario,$user);
                    array_push($usuario,$email);
                    array_push($usuario,"");
                    array_push($usuario,$user);
                    array_push($usuario,$token);
                    array_push($usuario,$passwrd);
                    array_push($usuario,"0");
                    $save = loadModel(MODEL_LOGIN, "login_model", "save_login", $usuario);
                    if ($save){
                        array_push($retorno, "Revise su correo");

                        $link ="http://127.0.0.1/programacio/FW-PHP-OO-AngularJS/#/login/active_account/$token";

                        $config = array();
                        $config['api_key'] = "key-5a273cbca614afd30b20de3fa94ef5f7"; //API Key
                        $config['api_url'] = "https://api.mailgun.net/v3/sandbox4e700072ff9c4da68f91d061b8bd0d51.mailgun.org/messages"; //API Base URL

                        $message = array();
                        $message['from'] = "cesar@equipocrypto.com";
                        $message['to'] = $email;
                        $message['h:Reply-To'] = "$email";
                        $message['subject'] = "Activacion cuenta";
                        $message['html'] = 'Hola ' . $email . ',<br></br>Active su cuenta pulsando en el link mostrado:<br></br>' . $link;
                     
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $config['api_url']);
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_POST, true); 
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }else{
                        array_push($retorno, "Ha habido un error en el registro");
                    }
                }
            }else{
                array_push($retorno, "El usuario ya existe");
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }


        function log_out(){
            $retorno=[];
            $_SESSION['usuario']="Guest";
            $_SESSION['login']="";
            $_SESSION['imagen']=IMG_PATH."login.png";
            $mensaje="LogOut succefull";
            array_push($retorno,$mensaje,$_SESSION['imagen']);
            echo json_encode($retorno);

        }

        function active_account(){
                $token=json_decode($_GET['param']);

                $activate = loadModel(MODEL_LOGIN, "login_model", "activate", $token);
                if ($activate){
                    echo true;
                }else{
                    echo false;
                }
                
        }

        function reset_account(){
            $usuario=json_decode($_GET['param']);
            $retorno = [];
            $busqueda = [];
            $email=$usuario[0];
            $pass=$usuario[1];
            $passwrd = hash("sha256" , $pass);
            $_SESSION['pass']=$passwrd;
            array_push($busqueda, "email");
            array_push($busqueda, $email);

            $findemail = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            $id=$findemail[0]['idusuario'];
            if ($findemail){
                $usuario = [];

                $token = sha1(mt_rand(1, 90000) . 'SALT');
                array_push($usuario, $id);
                array_push($usuario, $token);

                $save_token = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($save_token){
                    array_push($retorno, "Revise su correo");
                    $link ="http://127.0.0.1/programacio/FW-PHP-OO-AngularJS/#/login/active_reset/$token";


                    $config = array();
                    $config['api_key'] = "key-5a273cbca614afd30b20de3fa94ef5f7"; //API Key
                    $config['api_url'] = "https://api.mailgun.net/v3/sandbox4e700072ff9c4da68f91d061b8bd0d51.mailgun.org/messages"; //API Base URL

                    $message = array();
                    $message['from'] = "cesar@equipocrypto.com";
                    $message['to'] = $email;
                    $message['h:Reply-To'] = "$email";
                    $message['subject'] = "Cambio contraseña";
                    $message['html'] = 'Hola ' . $email . ',<br></br>Pulse en el siguiente link para confirmar el cambio de la contraseña:<br></br>' . $link;
                 
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_POST, true); 
                    curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
                    $result = curl_exec($ch);
                    curl_close($ch);
                }
            }

            echo json_encode($retorno);
        }
        function active_reset(){
                $usuario['usuario'] = [];

                $token=json_decode($_GET['param']);
                $pass=$_SESSION['pass'];

                array_push($usuario, $pass);
                array_push($usuario, $token);


                $activate = loadModel(MODEL_LOGIN, "login_model", "reset_pass", $usuario);
                if ($activate){
                    echo true;
                }else{
                    echo false;
                }
                
        }

}
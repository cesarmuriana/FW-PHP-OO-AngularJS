<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define(SITE_ROOT, $path);
define('MODEL_PATH', SITE_ROOT . 'model/');

require(MODEL_PATH . "Db.class.singleton.php");
require(SITE_ROOT . "modules/login/model/DAO/login_dao.class.singleton.php");

class login_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function save_login_bll($user){
      return $this->dao->save_login_dao($this->db,$user);
    }
    public function search_user_bll($user){
      return $this->dao->search_user_dao($this->db,$user);
    }
    public function login_user_bll($user){
      return $this->dao->login_user_dao($this->db,$user);
    }
    public function activate_bll($user){
      return $this->dao->activate_dao($this->db,$user);
    }
    public function reset_pass_bll($user){
      return $this->dao->reset_pass_dao($this->db,$user);
    }
}
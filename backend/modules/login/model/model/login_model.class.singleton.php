<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define('SITE_ROOT', $path);

require( SITE_ROOT . 'modules/login/model/BLL/login_bll.class.singleton.php');

class login_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function save_login($user){
        return $this->bll->save_login_bll($user);
    }

    public function search_user($user){
        return $this->bll->search_user_bll($user);
    }

    public function login_user($user){
        return $this->bll->login_user_bll($user);
    }
    public function activate($user){
        return $this->bll->activate_bll($user);
    }
    public function reset_pass($user){
        return $this->bll->reset_pass_bll($user);
    }
}


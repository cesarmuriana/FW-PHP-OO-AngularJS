<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define(SITE_ROOT, $path);
define('MODEL_PATH', SITE_ROOT . 'model/');

require(MODEL_PATH . "Db.class.singleton.php");
require(SITE_ROOT . "modules/profile/model/DAO/profile_dao.class.singleton.php");

class profile_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = profile_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function search_user_bll($user){
      return $this->dao->search_user_dao($this->db,$user);
    }
    public function save_user_bll($user){
      return $this->dao->save_user_dao($this->db,$user);
    }
}
<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define('SITE_ROOT', $path);

require( SITE_ROOT . 'modules/profile/model/BLL/profile_bll.class.singleton.php');

class profile_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = profile_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function search_user($user){
        return $this->bll->search_user_bll($user);
    }
    public function save_user($user){
        return $this->bll->save_user_bll($user);
    }
}


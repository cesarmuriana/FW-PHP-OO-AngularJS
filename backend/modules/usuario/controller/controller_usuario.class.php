<?php
/*session_start();
include ($_SERVER['DOCUMENT_ROOT'] . "/programacio/FW-PHP-OO-JQuery/modules/usuario/utils/functions_usuario.inc.php");
include ($_SERVER['DOCUMENT_ROOT'] . "/programacio/FW-PHP-OO-JQuery/utils/upload.php");
include ($_SERVER['DOCUMENT_ROOT'] . "/programacio/FW-PHP-OO-JQuery/utils/common.inc.php");
*/
class controller_usuario{
  function __construct() {
      //include(UTILS . "common.inc.php");
  }
  function create_user(){
    
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");

      loadView(VIEW_USUARIO . 'create_usuario.html');

      require_once(VIEW_PATH_INC . "footer.html");
  }

  /*-------------------------- UPLOAD ------------------------- */

  function upload(){
    $result_prodpic = upload_files();
    $_SESSION['result_prodpic'] = $result_prodpic;
      //echo json_encode($result_prodpic);
  }

  /*-------------------------- ALTA_USERS ------------------------- */

  function alta_usuario(){
    $error= array();

    $path_model = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/modules/usuario/model/model/';

    $jsondata = array();
    $search_referal = array();

    $usuarioJSON = json_decode($_POST["data_user_JSON"], true);
    /* print_r($usuarioJSON);
    die(); */

    if (empty($_SESSION['result_prodpic'])){
        $_SESSION['result_prodpic'] = array('result' => true, 'error' => "", "data" => "/programacio/FW-PHP-OO-JQuery/media/default-avatar.png");
    }
    $result_prodpic = $_SESSION['result_prodpic'];

    $search_user = loadModel($path_model, "usuario_model", "search_user", $usuarioJSON['nameuser']);

    if ($search_user){
      $error='username';
      print_r($error);
      die();
    }
    
    $search_referal = loadModel($path_model, "usuario_model", "search_user", $usuarioJSON['referraluser']);

    if (!$search_referal){
      $error='referal_user';
      print_r($error);
      die();
    }


    $today_data = date("d/m/Y");
    $initial_data = date($usuarioJSON['date_reception']);
    $expiration_data = date($usuarioJSON['date_expiration']);


    if ($today_data >= $initial_data ){
      $error="low_date";
      print_r($error);
      die();
    }
      
    if ( $initial_data > $expiration_data ){
      $error="hig_date";
      print_r($error);
      die();
    }

    $interval = abs($initial_data - $expiration_data);

    if ( $interval < 4 ){
      $error="minium_range";
      print_r($error);
      die();
    }

    try{
      $arrValue = false;
      $arrValue = loadModel($path_model, "usuario_model", "create_user", $usuarioJSON);
      $_SESSION['user'] = $usuarioJSON;
      $callback="index.php?module=usuario&view=results_usuario";
      $jsondata['redirect'] = $callback;
      $jsondata['error']='false';
      echo json_encode($jsondata);
      die();
    } catch (Exception $e) {
      $jsondata['error']='true'; 
      $jsondata['mensaje']="Ha habido un error al insertar el usuario en la base de datos";
      echo json_encode($error);
      die();
    }
  }//End alta products
  /* --------------------- GET USER ------------------------- */

  function get_user(){

    $user=$_SESSION['user'];
    echo json_encode($user);
    die();
  }

  /*-------------------------- DELETE ---------------------------*/

  function delete(){
    //echo json_encode("Hello world from delete in controller_products.class.php");
    $_SESSION['result_prodpic'] = array();
    $result = remove_files();
    if($result === true){
      echo json_encode(array("res" => true));
    }else{
      echo json_encode(array("res" => false));
    }
    //echo json_decode($result);
  }

  /* -------------------------- LOAD ----------------------------*/

  function load(){
      $jsondata = array();
      if (isset($_SESSION['product'])) {
          //echo debug($_SESSION['user']);
          $jsondata["product"] = $_SESSION['product'];
      }
      if (isset($_SESSION['message'])) {
          //echo $_SESSION['msje'];
          $jsondata["message"] = $_SESSION['message'];
      }
      close_session();
      echo json_encode($jsondata);
      exit;
  }

  function close_session() {
      unset($_SESSION['product']);
      unset($_SESSION['message']);
      $_SESSION = array(); // Destruye todas las variables de la sesión
      session_destroy(); // Destruye la sesión
  }

  /* ----------------------------- LOAD_DATA ---------------------------- */

  function load_data(){
      $jsondata = array();

      if (isset($_SESSION['product'])) {
          $jsondata["product"] = $_SESSION['product'];
          echo json_encode($jsondata);
          exit;
      } else {
          $jsondata["product"] = "";
          echo json_encode($jsondata);
          exit;
      }
  }

  /* ------------------------- LOAD_COUNTRY --------------------------- */

    function load_country(){

      $url = '/programacio/FW-PHP-OO-JQuery/resources/ListOfCountryNamesByName.json'; /* path to your JSON file */
  		if($url){
  			print_r($url);
  			exit;
  		}else{
  			$url = "error";
  			print_r($url);
  			exit;
  		}

  	}

  /* Sols queda pasar la id de la provincia i posarles provincies en JS i tornar les cities i pintarles amb JS */

  /* ---------------------------load_provinces ------------------------- */

    function load_provinces(){

      $arrayValue = array();

      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/modules/usuario/model/model/';
      $arrayValue = loadModel($path_model, "usuario_model", "get_provinces");
    
      echo json_encode($arrayValue);

      die();

    }

  /* ------------------------ load_cities ------------------------------*/

    function load_poblacion(){

  	  $arrayValue = array();

  		$path_model = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/modules/usuario/model/model/';
  		$arrayValue = loadModel($path_model, "usuario_model", "get_cities", $_POST['eleccion']);

      echo json_encode($arrayValue);

      die();
  	}
}/* End controller usuario */
<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define(SITE_ROOT, $path);
define('MODEL_PATH', SITE_ROOT . 'model/');

require(MODEL_PATH . "Db.class.singleton.php");
require(SITE_ROOT . "modules/usuario/model/DAO/usuario_dao.class.singleton.php");

class usuario_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = usuario_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_user_bll($usuario){
      return $this->dao->create_user_dao($this->db, $usuario);
    }

    public function obtain_countries_BLL($url){
      return $this->dao->obtain_countries_DAO($url);
    }

    public function get_provinces_dll(){
      return $this->dao->get_provinces_dao($this->db);
    }

    public function get_cities_bll($id_province){
      return $this->dao->get_cities_dao($this->db, $id_province);
    }

    public function search_user_bll($user){
      return $this->dao->search_user_dao($this->db, $user);
    }
    
}
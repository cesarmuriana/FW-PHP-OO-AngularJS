<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define('SITE_ROOT', $path);

require( SITE_ROOT . 'modules/usuario/model/BLL/usuario_bll.class.singleton.php');

class usuario_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = usuario_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_user($usuario) {
        return $this->bll->create_user_bll($usuario);
    }

    public function obtain_countries($url){
        return $this->bll->obtain_countries_BLL($url);
    }

    public function get_provinces(){
        return $this->bll->get_provinces_dll();
    }

    public Function get_cities($id_province){
        return $this->bll->get_cities_bll($id_province);
    }

    public Function search_user($user){
        return $this->bll->search_user_bll($user);
    }
}
var eventplanner = angular.module('eventplanner',['ngRoute','ui.bootstrap'/*, 'ngAnimate', 'ngCookies', 'facebook'*/]);
eventplanner.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                /* Home */
                .when("/", {
                    templateUrl: "frontend/modules/home/view/home.view.html", 
                    controller: "homeCtrl"
                })

                .when("/home", {
                    templateUrl: "frontend/modules/home/view/home.view.html", 
                    controller: "homeCtrl"
                })

                /* Shop */
                .when("/shop", {
                    templateUrl: "frontend/modules/home/view/shop.view.html", 
                    controller: "shopCtrl"
                })

                /* Contact */
                .when("/contact", {
                    templateUrl: "frontend/modules/contact/view/contact.view.html", 
                    controller: "contactCtrl"
                })

                /* Profile */
                .when("/user/profile", {
                    templateUrl: "frontend/modules/profile/view/profile.view.html", 
                    controller: "profileCtrl",
                    resolve: {
                        prof_user: function (services, servicelocalstorage){
                                var token = servicelocalstorage.get();
                                return servicelocalstorage.getuser(token);
                        }
                    }
                })

                /* Login */
                .when("/login", {
                    templateUrl: "frontend/modules/login/view/login.view.html", 
                    controller: "loginCtrl"
                })

                /* Active account */
               .when("/login/active_account/:token", {
                    templateUrl: "frontend/modules/login/view/login.view.html", 
                    controller: "verifyCtrl"
                })

                .when("/user/reset", {
                    templateUrl: "frontend/modules/login/view/reset.view.html", 
                    controller: "resetCtrl"
                })

                /* Active reset password */
               .when("/login/active_reset/:token", {
                    templateUrl: "frontend/modules/login/view/login.view.html", 
                    controller: "verifyresetCtrl"
                })

                /* Monedas */
                .when("/moneda/:criptomoneda", {
                    templateUrl: "frontend/modules/home/view/list_details.view.html", 
                    controller: "detailsCtrl",
                    resolve: {
                        criptomoneda: function (services, $route){
                            services.get("home","popularity_crypto",$route.current.params.criptomoneda).then(function (response) {
                            });
                            return $route.current.params.criptomoneda;
                        }
                    }
                })

                /* List monedas by categoria */
                .when("/home/list_categoria/:categoria", {
                    templateUrl: "frontend/modules/home/view/list_result.view.html", 
                    controller: "listCtrl",
                    resolve: {
                        categoria: function (services, $route){
                                return $route.current.params.categoria;
                        }
                    }
                })

                // else 404
                .otherwise("/", {templateUrl: "frontend/assets/inc/404.view.html"});
    }]);
eventplanner.factory("CommonService", ['$rootScope','$timeout','services', function ($rootScope, $timeout, services) {
        var service = {};
        service.banner = banner;
        service.amigable = amigable;
        service.busqueda_moneda = busqueda_moneda;
        return service;

        function banner(message, type) {
            $rootScope.bannerText = message;
            $rootScope.bannerClass = 'alertbanner aletbanner' + type;
            $rootScope.bannerV = true;

            $timeout(function () {
                $rootScope.bannerV = false;
                $rootScope.bannerText = "";
            }, 5000);
        }
        
        function amigable(url) {
            var link = "";
            url = url.replace("?", "");
            url = url.split("&");

            for (var i = 0; i < url.length; i++) {
                var aux = url[i].split("=");
                link += aux[1] + "/";
            }
            return link;
        }
        
        function busqueda_moneda(moneda) {
            /* Comprueba si existe en BD si no "Esta criptomoneda no existe" */
          services.get('home', 'get_moneda_by_name', moneda).then(function (response) {
            try{
                var nombre = response[0].nombre;
                window.location.replace("#/moneda/"+nombre);
            }catch(err){
                toastr.error("Esa criptomoneda no existe");
            }
          });
        };
    }]);

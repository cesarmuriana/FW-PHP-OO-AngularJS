eventplanner.controller('contactCtrl', function ($scope, services) {

    $scope.enviar_contact = function() {
      var usuario = [];

      var email = $scope.contact_email;
      var texto = $scope.contact_text;
      var evento = $scope.inputSubject_contact;

      usuario.push(email);
      usuario.push(texto);
      usuario.push(evento);

      services.post('contact', 'send_contact', usuario).then(function (response) {
        var response = JSON.parse(response);
        if (response === true){
          toastr.success("Consulta enviada correctamente");
        }else{
          toastr.error("Ha habido un fallo con su consulta");
        }

      });

  	};
});
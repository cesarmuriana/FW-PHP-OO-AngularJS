eventplanner.controller('detailsCtrl', function ($scope, services, CommonService, criptomoneda, servicelocalstorage) {

var crypto = [];
rating = [];
rating.push("1");
rating.push("2");
rating.push("3");
rating.push("4");
rating.push("5");
$scope.rate = rating;

    services.get_moneda(criptomoneda).then(function (response) {

    	var datos = response;
        try{
    		getpriceur().then(function(usdtoeur) {

    			var usd=datos[0].price_usd;
	    		var cap = datos[0].market_cap_usd;
	    		var max_cap=datos[0].max_supply;
    			var prieur=usd/usdtoeur;
    			var cap_eur=cap/usdtoeur;
    			var max_eur=max_cap/usdtoeur;


	            var crypto = [prieur.toFixed(4),cap_eur.toFixed(2),max_eur.toFixed(2)]

	            /*
	            crypto.push(data[0].name);
	            crypto.push(data[0].price_btc);
	            crypto.push(data[0].percent_change_7d);
	            crypto.push(data[0].percent_change_24h);
	            crypto.push(data[0].percent_change_1h);
	            crypto.push(data[0].rank);*/
	            $scope.crypto = crypto;
	            $scope.datos = datos;
	            //console.log($scope.crypto);
	        });
	    }catch(err){
	    	toastr.error("Ha surgido un error al recoger los datos de la moneda");
		}
    });
		
    /* Recoge todas las monedas para el buscador */
	services.get('home', 'get_all_monedas').then(function (response) {
		$scope.monedas = response;
    });
    
	$scope.complete = function(string){
		$scope.hidethis = false;
		var output = [];
		angular.forEach($scope.monedas, function(moneda){
			if (moneda.nombre.toLowerCase().indexOf(string.toLowerCase()) >= 0){
				output.push(moneda.nombre_web);
			}
			$scope.filterMoneda = output;
		});
	}

	$scope.fillTextBox = function(string){
		$scope.busqueda_cripto = string;
		$scope.hidethis = true;
	}

	$scope.busqueda_moneda = function() {
		var moneda = $scope.busqueda_cripto;
		CommonService.busqueda_moneda(moneda);
  	};


    function getpriceur() {
    	const promise = new Promise(function(resolve, reject) {
	    	services.cambio_eur().then(function (response) {
	    		resolve(response);
	      	});
	    });
       	return promise
    };

    $scope.likecrypto = function(){

    	var enviar = [];
    	enviar.push({criptomoneda});
    	var token = servicelocalstorage.get();
    	if (token){
	    	servicelocalstorage.getuser(token).then(function(user) {
	    		enviar.push(user[0]);
	    		enviar = JSON.stringify(enviar);
	    		services.get('home', 'set_likes', enviar).then(function (response) {
	    			if (response[1] == true){
	    				toastr.success(response[0]);
	    			}else{
	    				toastr.error(response[0]);
	    			}

	   			});
	    	});
		}else{
			toastr.error("Tienes que estar logueado");
		}
    }


    $scope.rating = function(string){
    	console.log();
    	var valoracion = string.data;

		var enviar = [];
    	enviar.push({criptomoneda});
    	var token = servicelocalstorage.get();
    	if (token){
	    	servicelocalstorage.getuser(token).then(function(user) {
	    		enviar.push(user[0]);
	    		enviar.push(valoracion);
	    		enviar = JSON.stringify(enviar);
	    		services.get('home', 'set_rate', enviar).then(function (response) {

	    			if (response[1] == true){
	    				toastr.success(response[0]);
	    				detalles_rate();
	    			}else{
	    				toastr.error(response[0]);
	    			}

	   			});
	    	});
		}else{
			toastr.error("Tienes que estar logueado");
		}

    }
    detalles_rate();
    /* count rates */
    function detalles_rate(){
		services.get('home', 'count_rates', criptomoneda).then(function (response) {
			$scope.cantidad= response[0];
			$scope.media= response[1];
		});
	}

    $scope.comment = function(){
    	console.log("comments");
/*
    	var enviar = [];
    	enviar.push({criptomoneda});
    	var token = servicelocalstorage.get();
    	if (token){
	    	servicelocalstorage.getuser(token).then(function(user) {
	    		enviar.push(user[0]);
	    		enviar = JSON.stringify(enviar);
	    		services.get('home', 'set_likes', enviar).then(function (response) {
	    			if (response[1] == true){
	    				toastr.success(response[0]);
	    			}else{
	    				toastr.error(response[0]);
	    			}

	   			});
	    	});
		}else{
			toastr.error("Tienes que estar logueado");
		}*/
    }
});/* END CONTROLLER */
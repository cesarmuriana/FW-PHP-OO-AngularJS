eventplanner.controller('homeCtrl', function ($scope, services, CommonService) {
	var cat=3;
	var categoria = [];
	var todas_categorias = [];
	var datos = [];
	var catego = [];

	$scope.seecat = function(string){
		catego = string.data.cod_cat;
		categories_cat(catego);
		window.location.replace("#/home/list_categoria/"+catego);
	}

   	services.get('home', 'get_categories').then(function (response) {
   		datos=response;
   		count=0;
   		todas_categorias = response;
   		categoria.push(response[0]);
   		categoria.push(response[1]);
   		categoria.push(response[2]);
   		categoria.push(response[3]);
   		$scope.categoria = categoria;
  	});

	/* Recoge todas las monedas para el buscador */
	services.get('home', 'get_all_monedas').then(function (response) {
		$scope.monedas = response;
    });
    
	$scope.complete = function(string){
		$scope.hidethis = false;
		var output = [];
		angular.forEach($scope.monedas, function(moneda){
			if (moneda.nombre.toLowerCase().indexOf(string.toLowerCase()) >= 0){
				output.push(moneda.nombre_web);
			}
			$scope.filterMoneda = output;
		});
	}

	$scope.fillTextBox = function(string){
		$scope.busqueda_cripto = string;
		$scope.hidethis = true;
	}

	$scope.busqueda_moneda = function() {
		var moneda = $scope.busqueda_cripto;
		CommonService.busqueda_moneda(moneda);
  	};

	$scope.morecat = function (){
			if (todas_categorias.length==categoria.length){
				toastr.error("No quedan mas categorias");
			}else{
				cat++;
    			categoria.push(datos[cat]);
    			cat++;
    			categoria.push(datos[cat]);
			}
		};
		

	function categories_cat(clase){
        services.get('home', 'popularity_categories', clase).then(function (response) {
	    });
	}

});/* END HOME CONTROLLER */
eventplanner.controller('listCtrl', function ($scope, services, CommonService, categoria, events_map) {

	$scope.seemoneda = function(string){
		var nombre = string.data.nombre;
		CommonService.busqueda_moneda(nombre);
	}
		
	services.get('home', 'get_monedas_by_cat',categoria).then(function (response) {
		events_map.initMap(response);
		$scope.todas_monedas = response;
		$scope.muestra_monedas = $scope.todas_monedas.slice(0, 4);
    });

	/* Recoge todas las monedas para el buscador */
	services.get('home', 'get_all_monedas').then(function (response) {
		$scope.monedas = response;
    });

	$scope.pageChanged = function() {
	  var startPos = ($scope.currentPage - 1) * 4;
	  $scope.muestra_monedas = $scope.todas_monedas.slice(startPos, startPos + 4);
	};

	$scope.complete = function(string){
		$scope.hidethis = false;
		var output = [];
		angular.forEach($scope.monedas, function(moneda){
			if (moneda.nombre.toLowerCase().indexOf(string.toLowerCase()) >= 0){
				output.push(moneda.nombre_web);
			}
			$scope.filterMoneda = output;
		});
	}

	$scope.fillTextBox = function(string){
		$scope.busqueda_cripto = string;
		$scope.hidethis = true;
	}

	$scope.busqueda_moneda = function() {
		var moneda = $scope.busqueda_cripto;
		CommonService.busqueda_moneda(moneda);
  	};
}); /* END CONTROLLER */
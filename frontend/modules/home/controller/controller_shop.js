eventplanner.controller('shopCtrl', function ($scope, services, CommonService) {

		$scope.seemoneda = function(string){
			var nombre = string.data.nombre;
	        CommonService.busqueda_moneda(nombre);
		}

		/* Recoge todas las monedas para el buscador */
		services.get('home', 'get_all_monedas').then(function (response) {
			$scope.monedas = response;
			$scope.muestra_monedas = $scope.monedas.slice(0, 4);
	    });

		$scope.pageChanged = function() {
		  var startPos = ($scope.currentPage - 1) * 4;
		  $scope.muestra_monedas = $scope.monedas.slice(startPos, startPos + 4);
		};
        
		$scope.complete = function(string){
			$scope.hidethis = false;
			var output = [];
			angular.forEach($scope.monedas, function(moneda){
				if (moneda.nombre.toLowerCase().indexOf(string.toLowerCase()) >= 0){
					output.push(moneda.nombre_web);
				}
				$scope.filterMoneda = output;
			});
		}

		$scope.fillTextBox = function(string){
			$scope.busqueda_cripto = string;
			$scope.hidethis = true;
		}

		$scope.busqueda_moneda = function() {
			var moneda = $scope.busqueda_cripto;
			CommonService.busqueda_moneda(moneda);
	  	};

		/*function back(){
        	window.location.replace("../../home/index/");
        }*/
	}); /* END READY */
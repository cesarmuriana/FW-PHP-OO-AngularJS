eventplanner.factory("events_map", ['$rootScope','$timeout','services', function ($rootScope, $timeout, services) {
	var service = {};
	service.initMap = initMap;
	service.marcar=marcar;
	return service;

	function initMap(categoria){
		
		
		var locations = [];
		
		categoria.forEach(function(element) {
			var data = [];
			data.push(element.nombre_web);
			data.push(element.long);
			data.push(element.lat);
			locations.push(data);
		});

        var map = new google.maps.Map(document.getElementById('map'), {
	    	zoom: 2,
	    	center: new google.maps.LatLng(0,0),
	    	mapTypeId: google.maps.MapTypeId.ROADMAP
	    });
	        $rootScope.map = map;
            for (var i = 0; i < locations.length; i++) {
                marcar(map, locations[i]);
            }
	}

	function marcar (map, event) {
        var latlon = new google.maps.LatLng(event[1], event[2]);
        var marker = new google.maps.Marker({position: latlon, map: map, title: event[0], animation: null});

        marker.set('id', event[0]);
        marker.set('latlon', latlon);
    }

}]);
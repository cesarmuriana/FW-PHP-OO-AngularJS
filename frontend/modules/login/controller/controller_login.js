eventplanner.controller('loginCtrl', function ($scope, services, CommonService, socialLogin, servicelocalstorage) {

  $scope.twitter = function(){
    socialLogin.logintwitter();
  }

  $scope.google = function(){
    socialLogin.logingoogle();
  }

  $scope.ownlogin = function(){
    var usuario =  $scope.usuario_login;
    var passwrd = $scope.pass_login;
    socialLogin.ownlogin(usuario,passwrd);
  }

  $scope.ownregister = function(){
    var usuario = $scope.usuario_register;
    var email = $scope.email_register;
    var passwrd = $scope.pass_register;
    socialLogin.ownregister(usuario,email,passwrd);
  }
});

eventplanner.controller('verifyCtrl', function ($scope, services, CommonService, $route, servicelocalstorage, servicesMenu) {
    var token = $route.current.params.token;
    if (token.substring(0, 3) !== 'Ver') {
        CommonService.banner("Ha habido algún tipo de error con la dirección", "Err");
        //$location.path('/');
    }
    var token = JSON.stringify(token);
    services.get("login", "active_account", token).then(function (response) {

        if (response == true) {
            servicelocalstorage.set(token);
            toastr.success("Su cuenta ha sido satisfactoriamente verificada");
            servicesMenu.chekmenu(token);
            setTimeout(function(){
              window.location.replace("#/home/");
            }, 1000);
        } else {
          toastr.error("Ha habido un error");
        }
    });
});
eventplanner.controller('resetCtrl', function ($scope, services, CommonService, socialLogin, servicelocalstorage) {
  

  $scope.reset_passwrd = function(){
    var email = $scope.email_reser;
    var passwrd = $scope.pass_reset;
    socialLogin.resetpass(email,passwrd);
  }
});

eventplanner.controller('verifyresetCtrl', function ($scope, services, CommonService, $route, servicelocalstorage, servicesMenu) {
    
    var token = $route.current.params.token;

    if (token.substring(0, 3) !== 'Ver') {
        CommonService.banner("Ha habido algún tipo de error con la dirección", "Err");
    }

    var token = JSON.stringify(token);
    services.get("login", "active_reset", token).then(function (response) {

        if (response == true) {
            servicelocalstorage.set(token);
            toastr.success("Su cuenta ha cambiado de contraseña satisfactoriamente");
            servicesMenu.chekmenu(token);
            setTimeout(function(){
              window.location.replace("#/home/");
            }, 1000);

        } else {
          toastr.error("Ha habdido un error en el cambio de contraseña");
        }
    });
});

eventplanner.factory("servicelocalstorage", ['$rootScope','$timeout','services','CommonService', function ($rootScope, $timeout, services, CommonService) {
		var service = {};
        service.get = get;
        service.delate = delate;
        service.getuser = getuser;
        service.set = set;
        return service;

        function get(){
        	token = localStorage.log_user;
        	return token;
        }

        function getuser(token){
                const promise = new Promise(function(resolve, reject) {
                        services.get('login', 'getuserbytoken',token).then(function (response) {
                                resolve(response);
                        });
                });
                return promise

        }

        function delate(){
        	localStorage.removeItem('log_user');
        }

        function set(token){
        	localStorage.log_user=token;
        }
}]);
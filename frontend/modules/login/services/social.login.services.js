eventplanner.factory("socialLogin", ['$rootScope','$timeout','services','CommonService','servicelocalstorage','servicesMenu', 
	function ($rootScope, $timeout, services, CommonService, servicelocalstorage, servicesMenu) {
		
 var config = {
    apiKey: "AIzaSyDwGUKpCS4Fi94h6cpXOnxR6FqK-zemuM4",
    authDomain: "fw-php-oo-angularjs.firebaseapp.com",
    databaseURL: "https://fw-php-oo-angularjs.firebaseio.com",
    projectId: "fw-php-oo-angularjs",
    storageBucket: "fw-php-oo-angularjs.appspot.com",
    messagingSenderId: "875959956282"
  };
  firebase.initializeApp(config);

		var service = {};
        service.logintwitter = logintwitter;
        service.logingoogle = logingoogle;
        service.ownlogin = ownlogin;
        service.ownregister = ownregister;
		service.resetpass = resetpass;
        return service;



        function logintwitter(){
			var provider = new firebase.auth.TwitterAuthProvider();
	      	var authService = firebase.auth();

	        authService.signInWithPopup(provider).then(function(result) {
	            var usuario = [];
	            var username = result.user.displayName;
	            var img = result.user.photoURL;
	            var uid = result.user.uid;
	            var name = '';
	            var email = '';
	            usuario.push(name);
	            usuario.push(username);
	            usuario.push(email);
	            usuario.push(img);
	            usuario.push(uid);
	            var usuario = JSON.stringify(usuario);
	            services.get('login', 'login_twiter',usuario).then(function (response) {
	            	try{
                        var data = response;
		            	var token = JSON.stringify(data[0]);
		                servicelocalstorage.set(token);
		                var user = data[2];
		                $rootScope.usuario= user;
		                toastr.success(data[1]);
		                servicesMenu.chekmenu(token);
		                setTimeout(function(){
		                  window.location.replace("#/home/");
		                }, 1000);
					}catch(err){
                        toastr.success("Error login");
                    }
	            });
	        }).catch(function(error) {
				toastr.error('Se ha encontrado un error:', error);
	    	});

        }

       	function logingoogle(){
       		var provider = new firebase.auth.GoogleAuthProvider();
        	provider.addScope('email');
      
          	var authService = firebase.auth();

	        // manejador de eventos para loguearse
	        document.getElementById('logingoogle').addEventListener('click', function() {
            authService.signInWithPopup(provider)
                .then(function(result) {
                    var usuario = [];
                    var name = result.additionalUserInfo.profile.given_name;
                    var username = result.user.displayName;
                    var email = result.user.email;
                    var img = result.user.photoURL;
                    var uid = result.user.uid;
                    usuario.push(name);
                    usuario.push(username);
                    usuario.push(email);
                    usuario.push(img);
                    usuario.push(uid);
                    var usuario = JSON.stringify(usuario);
					services.get('login', 'login_google',usuario).then(function (response) {
                    	try{
                            var data = response;
                            var token = JSON.stringify(data[0]);
		                	servicelocalstorage.set(token);
                            var user = data[2];
                            $rootScope.usuario=user;
                            servicesMenu.chekmenu(token);
                            toastr.success(data[1]);
                            setTimeout(
                            function() 
                            {
                              window.location.replace("#/home/");
                            }, 1000);
                        }catch(err){
                        	toastr.success("Error login");
                      	}
                    });
                })
                .catch(function(error) {
                    toastr.error('Se ha encontrado un error:', error);
                });
          	})
          
       	}

		function ownlogin(user,pass){

			var usuario = [];
			
			usuario.push(user);
	        usuario.push(pass);

	        var usuario = JSON.stringify(usuario);
	        services.get('login', 'login_own',usuario).then(function (response) {
	            try{
	            	var data = response;
		            var user = data[2];
		            if (data[1] == "Login succefull"){
		            	toastr.success(data[1]);
		                var token = JSON.stringify(data[0]);
			            servicelocalstorage.set(token);
			            $rootScope.usuario= user;
                        servicesMenu.chekmenu(token);
		                setTimeout(
		                function() 
		                {
		                  window.location.replace("#/home");
		                }, 1000);
	              	}else{
	                	toastr.error("Combrueba el usuario y la contraseña");
	              	}
	            }catch(err){
	              toastr.error("Combrueba el usuario y la contraseña");
	            }
	        });
	    }

	    function ownregister(user,email,pass){
	    	var usuario = [];

	    	usuario.push(user);
	    	usuario.push(email);
	    	usuario.push(pass);
	    	var usuario = JSON.stringify(usuario);
	    	services.get('login', 'register_own',usuario).then(function (response) {
				try{
					console.log(response);
	                var data = response;
	                var user = data[1];
	                if (data[0]==="El usuario ya existe"){
                    toastr.error(data[0]);
                    setTimeout(
		                function() 
		                {
		                  toastr.error("Cambie el usuario");
		                }, 1000);
                    document.register_user.usuario_register.focus();
	                }else{
	                    if(data[0]==="El e-mail ya existe"){
	                        toastr.error(data[0]);
	                        setTimeout(
		                		function() 
		                		{
		                  			toastr.error("Cambie el e-mail");
		                		}, 1000);
	                        document.register_user.email_register.focus();
	                    }else{
	                      toastr.success(data[0]);
	                    }
	                }
                }catch(err){
                  toastr.error("Ha habido un error inesperado en el registro");
                }
	        });
	    }

	    function resetpass(email,pass){
	    	var usuario = [];

	    	usuario.push(email);
	    	usuario.push(pass);

	    	var usuario = JSON.stringify(usuario);
	    	services.get('login', 'reset_account',usuario).then(function (response) {
	    		console.log(response);
    			toastr.success(response[0]);
    		});
	    }

}]);
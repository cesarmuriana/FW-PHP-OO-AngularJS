eventplanner.controller('profileCtrl', 
	function ($scope, services, CommonService, servicelocalstorage, prof_user, servicesMenu, servicesProfile) {

	$scope.profile_usuario = prof_user;

	$scope.submit = function(){
		
		var token = servicelocalstorage.get();
		var usuario = [];
		var user = $scope.nameuser;
		var username = $scope.username;
		var surname = $scope.surname;
		var datebirthay = $scope.datebirthay;
		var email = $scope.email;

		if (user == undefined){
			user=prof_user[0].nameuser;
		}
		if (username == undefined){
			username=prof_user[0].username;
		}
		if (surname == undefined){
			surname=prof_user[0].surname;
		}
		if (datebirthay == undefined){
			datebirthay=prof_user[0].datebirthay;
		}
		if (email == undefined){
			email=prof_user[0].email;
		}

		usuario.push(token);
		usuario.push(user);
		usuario.push(username);
		usuario.push(surname);
		usuario.push(datebirthay);
		usuario.push(email);
		
		var usuario = JSON.stringify(usuario);
		services.get('profile', 'save_user',usuario).then(function (response) {
	    	if (response == true){
	    		toastr.success("El usuario se ha actualizado correctamente");
	    		var token = servicelocalstorage.get();
	    		servicesMenu.chekmenu(token);
	    		setTimeout(function(){
                  window.location.replace("#/home/");
                }, 1000);
	    	}else{
	    		toastr.error("Error al actualizar el usuario");
	    	}
        });
    
	}


	var enviar = [];
	var token = servicelocalstorage.get();
   	servicelocalstorage.getuser(token).then(function(user) {

   		/* GET LIKES */
		enviar.push(user[0]);
		enviar = JSON.stringify(enviar);
		services.get('home', 'get_likes', enviar).then(function (response) {
			var estado = response[response.length - 1];
			if (estado == true){
				response.splice(response.length - 1, 1);
				var monedas = [];
				response.forEach(function(element) {
				  monedas.push(element);
				});
				$scope.monedas = monedas; //ferli return appjs
			}else{
				toastr.error(response[0]);
			}
		});

		/* GET RATES */

		services.get('home', 'get_rate', enviar).then(function (response) {
			console.log(response);
			var estado = response[response.length - 1];
			if (estado == true){
				response.splice(response.length - 1, 1);
				var misrate = [];
				response.forEach(function(element) {
					console.log(element);
				  	misrate.push(element);
				});
				$scope.rating_profile = misrate; //ferli return appjs
			}else{
				toastr.error(response[0]);
			}
		});
	});



	/* servicesProfile.load_countries(); */
});
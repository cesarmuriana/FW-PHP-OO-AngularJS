eventplanner.factory("servicesProfile", ['$rootScope','services','CommonService','servicelocalstorage', 
	function ($rootScope, services, CommonService, servicelocalstorage) {

		var service = {};
        service.getuser = getuser;
        /*
        service.load_countries = load_countries;
        service.getprovinces = getprovinces;
        service.getpoblacion = getpoblacion;
        */
        return service;

        function getuser(token){
            const promise = new Promise(function(resolve, reject) {
                    services.get('profile', 'get_user',token).then(function (response) {
                            resolve(response);
                    });
            });
            return promise
        }
/*
        /* LOAD COUNTRIES 
        function load_countries() {
            services.get('countries').then(function (response) {
                try{
                    console.log("countries");
                    console.log(response);
                }catch(err){
                    services.get('profile','load_country').then(function (response) {

                    });
                }
            });
/*
            $.ajax({
                type: "GET",
                url: "https://api.printful.com/countries",
                success: function (data) {
                  var data = data.result;
                    $.each(data, function (i, valor) {
                        $("#country").append("<option value='" + valor.code + "'>" + valor.name + "</option>");
                    });
                },
                error: function () {
                    $.ajax({
                        type: "POST",
                        url: "../../usuario/load_country",
                        data: {load_country},
                        success: function (data) {
                          $.getJSON( data, function( data ) {
                            $.each(data, function (i, valor) {
                              $("#country").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
                            });
                          });
                        },
                        error: function () {
                          alert("Error on load countries");
                        }
                    });
                }
            });
          }

/* ------------------------------ LOAD_PROVINCES ------------------------------

        //$("#country").change(function(){
        function getprovinces(){
            var eleccion = document.getElementById("country").value;
            if ( eleccion === 'ES'){
              $.ajax({
                  type: "GET",
                  url: "https://api.printful.com/countriesasd",
                  success: function (data) {
                    /* No se encontro ninguna api (asi que se simula que el servidor no responde) 
                  },
                  error: function () {
                    $.ajax({
                        type: "POST",
                        url: "../../usuario/load_provinces",
                        success: function (data) {
                          try {
                            var provincias = JSON.parse(data);
                            $.each(provincias, function (i, valor) {
                              $("#province").append("<option value='" + valor.idprovincia + "'>" + valor.provinciaseo + "</option>");
                            });
                          }catch(err) {
                            alert("Error al cargar las provincias");
                          }
                        },
                        error: function () {
                          alert("Error al encontrar las provincias");
                        }
                    });
                  }
              });
            }else{
              /*$('.province').attr('disabled');
              $('#province').removeAttr('disabled');
              $('#city').removeAttr('disabled');
              alert("No tenemos acceso a provincias que no sean españolas");
            }
        };


  /* ------------------------------ LOAD_CITIES ------------------------------

          //$("#province").change(function(){
        function getpoblacion(){
            var eleccion = document.getElementById("province").value;
            services.get('home', 'get_all_monedas').then(function (response) {
                $scope.monedas = response;
                $scope.muestra_monedas = response;
            });
              $.ajax({
                  type: "GET",
                  url: "https://api.printful.com/countriesasd",
                  success: function (data) {
                    /* No se encontro ninguna api (asi que se simula que el servidor no responde) 
                  },
                  error: function () {
                    var load_poblacion = "true";
                    $.ajax({
                        type: "POST",
                        url: "../../usuario/load_poblacion",
                        data:{eleccion},
                        success: function (data) {
                          try {
                            var ciudades = JSON.parse(data);
                            $.each(ciudades, function (i, valor) {
                              $("#city").append("<option value='" + valor.idpoblacion + "'>" + valor.poblacionseo + "</option>");
                            });
                          }catch(err) {
                            alert("Error al cargar las ciudades");
                          }
                        },
                        error: function () {
                          alert("Error al encontrar las ciudades");
                        }
                    });
                }
                });
        };
*/
	}]);